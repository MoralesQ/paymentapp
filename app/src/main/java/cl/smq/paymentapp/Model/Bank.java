package cl.smq.paymentapp.Model;

import android.net.Uri;

public class Bank {

    private String id;
    private String name;
    private String sucureThumbnail;
    private String thumbnail;
    private boolean selected = false;



    public Bank() {
    }

    public Bank(String id, String name, String sucureThumbnail, String thumbnail, boolean selected) {
        this.id = id;
        this.name = name;
        this.sucureThumbnail = sucureThumbnail;
        this.thumbnail = thumbnail;
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSucureThumbnail() {
        return sucureThumbnail;
    }

    public void setSucureThumbnail(String sucureThumbnail) {
        this.sucureThumbnail = sucureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
