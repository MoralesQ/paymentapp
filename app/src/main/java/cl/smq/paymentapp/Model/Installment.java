package cl.smq.paymentapp.Model;

public class Installment {
    private String paymentMethodId;
    private String paymentTypeId;
    private int installments;
    private int minAllowedAmount;
    private int maxAllowedAmount;
    private String recommendedMessage;

    public Installment() {
    }

    public Installment(String paymentMethodId, String paymentTypeId, int installments, int minAllowedAmount, int maxAllowedAmount, String recommendedMessage) {
        this.paymentMethodId = paymentMethodId;
        this.paymentTypeId = paymentTypeId;
        this.installments = installments;
        this.minAllowedAmount = minAllowedAmount;
        this.maxAllowedAmount = maxAllowedAmount;
        this.recommendedMessage = recommendedMessage;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public int getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(int minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public int getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(int maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }

    @Override
    public String toString() {
        if (installments > 0){
            String in = (installments == 1 ? "Cuota": "Cuotas");
            return String.valueOf(installments) + " " + in;
        }else{
            return "Seleccione Cuota";
        }

    }
}
