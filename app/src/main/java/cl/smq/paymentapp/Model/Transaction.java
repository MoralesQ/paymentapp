package cl.smq.paymentapp.Model;
import java.util.UUID;

public class Transaction {

    private UUID id;
    private int amount;
    private PaymentMethod paymentMethod;
    private Bank bank;
    private Installment installment;

    public Transaction() {
    }

    public Transaction(int amount, PaymentMethod paymentMethod, Bank bank, Installment installment) {
        this.id = UUID.randomUUID();
        this.amount = amount;
        this.paymentMethod = paymentMethod;
        this.bank = bank;
        this.installment = installment;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBank(Bank bank) {
        this.bank = bank;
    }

    public Installment getInstallment() {
        return installment;
    }

    public void setInstallment(Installment installment) {
        this.installment = installment;
    }

    public String getTransaccionResume(){
        return "Monto: " + amount + "\n" + "Medio de pago: " + paymentMethod.getName() + "\n" + "Banco: " + bank.getName() + "\n" + "Cuotas: " + installment.getRecommendedMessage();
    }
}
