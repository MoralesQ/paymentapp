package cl.smq.paymentapp.Model;

import android.net.Uri;

import javax.sql.StatementEvent;

public class PaymentMethod {

    private String id;
    private String name;
    private String paymentTyeId;
    private String status;
    private String secureThumbnail;
    private String thumbnail;
    private int minAllowedAmount;
    private int maxAllowedAmount;
    private boolean selected = false;



    public PaymentMethod() {
    }

    public PaymentMethod(String id, String name, String paymentTyeId, String status, String secureThumbnail, String thumbnail, int minAllowedAmount, int maxAllowedAmount, boolean selected) {
        this.id = id;
        this.name = name;
        this.paymentTyeId = paymentTyeId;
        this.status = status;
        this.secureThumbnail = secureThumbnail;
        this.thumbnail = thumbnail;
        this.minAllowedAmount = minAllowedAmount;
        this.maxAllowedAmount = maxAllowedAmount;
        this.selected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTyeId() {
        return paymentTyeId;
    }

    public void setPaymentTyeId(String paymentTyeId) {
        this.paymentTyeId = paymentTyeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public void setSecureThumbnail(String secureThumbnail) {
        this.secureThumbnail = secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public int getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(int minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public int getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(int maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
