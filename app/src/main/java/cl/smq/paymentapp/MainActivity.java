package cl.smq.paymentapp;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import cl.smq.paymentapp.Presenter.MainActivityPresenter;
import cl.smq.paymentapp.View.MainActivityView;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    private AlertDialog dialog;
    private Fragment fragment;
    private MainActivityPresenter mainActivityPresenter;
    private Callbacks callbacks;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityPresenter = new MainActivityPresenter(this);
        mainActivityPresenter.attachView(this);
    }

    @Override
    public void setFragment(Fragment fragment) {
        this.fragment  = fragment;
        try{
            callbacks = (Callbacks) this.fragment;
        }catch (Exception e){
            callbacks = null;
        }
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mainFrameLayout, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        if(callbacks != null)
            callbacks.onBackPressedCallback();
        else{
            super.onBackPressed();
        }
    }

    @Override
    public void showDialog(AlertDialog.Builder dialogBuilder) {
        this.dialog = dialogBuilder.create();
        if (!this.isFinishing())
            this.dialog.show();
    }

    @Override
    public void dismissDialog() {
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    public interface Callbacks {
        public void onBackPressedCallback();
    }

    public void setMainFragment(){
        mainActivityPresenter.setMainFragment();
    }

}
