package cl.smq.paymentapp.View;

import android.app.AlertDialog;

import cl.smq.paymentapp.Adapter.PaymentMethodAdapter;
import cl.smq.paymentapp.Model.Transaction;

public interface PaymentMethodView {
    void setPaymentAdapter(PaymentMethodAdapter adapter);
    void showDialog(AlertDialog.Builder dialogBuilder);
    void dismissDialog();
    void backHome();
    void gotoBankFragment(Transaction transaction);
}
