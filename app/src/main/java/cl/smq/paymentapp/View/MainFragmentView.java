package cl.smq.paymentapp.View;

import android.app.AlertDialog;

import cl.smq.paymentapp.Model.Transaction;

public interface MainFragmentView {
    void showDialog(AlertDialog.Builder dialogBuilder);
    void dismissDialog();
    void goToPaymentMethodFragment(Transaction transaction);
    void backHome();
}
