package cl.smq.paymentapp.View;

import android.app.AlertDialog;
import android.widget.ArrayAdapter;

public interface InstallmentView {
    void setSpinnerAdapter(ArrayAdapter adapter);
    void showDialog(AlertDialog.Builder dialogBuilder);
    void showInstallmentMessage();
    void dismissDialog();
    void backHome();
}
