package cl.smq.paymentapp.View;

import android.app.AlertDialog;
import android.support.v4.app.Fragment;

public interface MainActivityView {
    void setFragment(Fragment fragment);
    void showDialog(AlertDialog.Builder dialogBuilder);
    void dismissDialog();
}
