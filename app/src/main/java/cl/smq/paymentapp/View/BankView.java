package cl.smq.paymentapp.View;
import android.app.AlertDialog;
import cl.smq.paymentapp.Adapter.BankAdapter;
import cl.smq.paymentapp.Model.Transaction;

public interface BankView {
    void setBankAdapter(BankAdapter adapter);
    void showDialog(AlertDialog.Builder dialogBuilder);
    void dismissDialog();
    void backHome();
    void gotoInstallmentsFragment(Transaction transaction);
}
