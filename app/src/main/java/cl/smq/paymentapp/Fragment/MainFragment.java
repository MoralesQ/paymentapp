package cl.smq.paymentapp.Fragment;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smq.paymentapp.MainActivity;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.Presenter.MainFragmentPresenter;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.View.MainFragmentView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements MainFragmentView, View.OnClickListener, MainActivity.Callbacks {
    View view;
    @BindView(R.id.amountInputLayout) TextInputLayout amountLayout;
    @BindView(R.id.amountButton) Button amountButton;
    MainFragmentPresenter mainFragmentPresenter;
    private AlertDialog dialog;
    public static MainFragment newInstance(){
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        mainFragmentPresenter = new MainFragmentPresenter(getActivity());
        mainFragmentPresenter.attachView(this);
        ButterKnife.bind(this, view);
        amountButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.amountButton:
                mainFragmentPresenter.validateAmount(amountLayout.getEditText().getText().toString());
        }
    }

    @Override
    public void showDialog(AlertDialog.Builder dialogBuilder) {
        this.dialog  = dialogBuilder.create();
        if (!getActivity().isFinishing()){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                }
            });
        }
    }

    @Override
    public void dismissDialog() {
        if (!getActivity().isFinishing() && dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void goToPaymentMethodFragment(Transaction transaction) {
        PaymentMethodFragment paymentMethodFragment = PaymentMethodFragment.newInstance();
        paymentMethodFragment.setTransaction(transaction);
        ((MainActivity)getActivity()).setFragment(paymentMethodFragment);
    }

    @Override
    public void backHome() {
        ((MainActivity)getActivity()).finish();
    }

    @Override
    public void onBackPressedCallback() {
        mainFragmentPresenter.showExitAlert();
    }
}
