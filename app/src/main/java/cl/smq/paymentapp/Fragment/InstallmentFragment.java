package cl.smq.paymentapp.Fragment;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;


import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smq.paymentapp.MainActivity;
import cl.smq.paymentapp.Model.Installment;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.Presenter.InstallmentPresenter;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.View.InstallmentView;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstallmentFragment extends Fragment implements InstallmentView, View.OnClickListener, MainActivity.Callbacks {

    View view;
    private AlertDialog dialog;
    private Transaction transaction;
    private Installment installment;
    InstallmentPresenter installmentPresenter;
    private boolean first = true;


    @BindView(R.id.installmentButton)Button installmentButton;
    @BindView(R.id.installmentSpinner) Spinner installmentSpinner;
    @BindView(R.id.installmentMessage) TextView installmentMessage;
    @BindView(R.id.progressBarInstallment) ProgressBar progressBar;


    public static InstallmentFragment newInstance(){
        InstallmentFragment fragment = new InstallmentFragment();
        return fragment;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            installmentPresenter.onResume();
        } catch (Exception e) {


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_installment, container, false);
        installmentPresenter = new InstallmentPresenter(getActivity(), transaction);
        installmentPresenter.attachView(this);
        ButterKnife.bind(this, view);
        installmentButton.setOnClickListener(this);
        installmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!first){
                    installment = (Installment) installmentSpinner.getAdapter().getItem(i);
                    installmentPresenter.validateInstallment(installment);
                }else {
                    first = false;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.installmentButton:
                installmentPresenter.showTransactionAlert();
                break;
        }
    }


    public void onBackPressedCallback() {
        installmentPresenter.showExitAlert();
    }


    @Override
    public void setSpinnerAdapter(ArrayAdapter adapter) {
        installmentSpinner.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialog(AlertDialog.Builder dialogBuilder) {
        this.dialog  = dialogBuilder.create();
        if (!getActivity().isFinishing()){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                }
            });
        }
    }

    @Override
    public void showInstallmentMessage() {
        installmentMessage.setVisibility(View.VISIBLE);
        installmentMessage.setText(installment.getRecommendedMessage());
        installmentButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void dismissDialog() {
        if (!getActivity().isFinishing() && dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void backHome() {
        ((MainActivity)getActivity()).setMainFragment();
    }
}
