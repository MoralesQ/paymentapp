package cl.smq.paymentapp.Fragment;


import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smq.paymentapp.Adapter.PaymentMethodAdapter;
import cl.smq.paymentapp.MainActivity;
import cl.smq.paymentapp.Model.PaymentMethod;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.Presenter.PaymentMethodPresenter;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.View.PaymentMethodView;

import static android.widget.LinearLayout.VERTICAL;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentMethodFragment extends Fragment implements PaymentMethodView, View.OnClickListener, MainActivity.Callbacks  {

    View view;
    PaymentMethodPresenter paymentMethodPresenter;
    private AlertDialog dialog;
    private Transaction transaction;
    private PaymentMethod paymentMethod;


    @BindView(R.id.paymentMethodRecycler) RecyclerView paymentRecycler;
    @BindView(R.id.paymentMethodButton) Button paymentButton;
    @BindView(R.id.progressBarPayment) ProgressBar progressBar;

    public static PaymentMethodFragment newInstance(){
        PaymentMethodFragment fragment = new PaymentMethodFragment();
        return fragment;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            paymentMethodPresenter.onResume();
        } catch (Exception e) {


        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_payment_method, container, false);
        paymentMethodPresenter = new PaymentMethodPresenter(getActivity());
        paymentMethodPresenter.attachView(this);
        ButterKnife.bind(this, view);
        paymentButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case R.id.paymentMethodButton:
                transaction.setPaymentMethod(paymentMethod);
                gotoBankFragment(transaction);
                break;
        }

    }

    @Override
    public void onBackPressedCallback() {
        paymentMethodPresenter.showExitAlert();
    }

    @Override
    public void setPaymentAdapter(PaymentMethodAdapter adapter) {
        adapter.setItemClickListener(new PaymentMethodAdapter.onRecyclerViewItemClickListener() {
            @Override
            public void onItemClickListener(View view, PaymentMethod selectedPaymentMethod) {
                paymentButton.setVisibility(View.VISIBLE);
                paymentMethod = selectedPaymentMethod;
            }
        });
        paymentRecycler.setHasFixedSize(false);
        paymentRecycler.setLayoutManager(new LinearLayoutManager(this.getActivity(), VERTICAL,false));
        paymentRecycler.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showDialog(AlertDialog.Builder dialogBuilder) {
        this.dialog  = dialogBuilder.create();
        if (!getActivity().isFinishing()){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                }
            });
        }
    }

    @Override
    public void dismissDialog() {
        if (!getActivity().isFinishing() && dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void backHome() {
        ((MainActivity)getActivity()).setMainFragment();
    }

    @Override
    public void gotoBankFragment(Transaction transaction) {
        BankFragment paymentMethodFragment = BankFragment.newInstance();
        paymentMethodFragment.setTransaction(transaction);
        ((MainActivity)getActivity()).setFragment(paymentMethodFragment);
    }


}
