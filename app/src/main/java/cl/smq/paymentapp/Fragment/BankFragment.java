package cl.smq.paymentapp.Fragment;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smq.paymentapp.Adapter.BankAdapter;
import cl.smq.paymentapp.MainActivity;
import cl.smq.paymentapp.Model.Bank;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.Presenter.BankFragmentPresenter;
import cl.smq.paymentapp.Presenter.PaymentMethodPresenter;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.DialogMaker;
import cl.smq.paymentapp.View.BankView;

/**
 * A simple {@link Fragment} subclass.
 */
public class BankFragment extends Fragment implements BankView, View.OnClickListener, MainActivity.Callbacks {

    View view;
    private AlertDialog dialog;
    private Transaction transaction;
    private Bank bank;
    BankFragmentPresenter bankFragmentPresenter;

    @BindView(R.id.bankRecycler) RecyclerView bankRecycler;
    @BindView(R.id.bankButton) Button bankButton;
    @BindView(R.id.progressBarBank) ProgressBar progressBar;

    public static BankFragment newInstance(){
        BankFragment fragment = new BankFragment();
        return fragment;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            bankFragmentPresenter.onResume();
        } catch (Exception e) {


        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_bank, container, false);
        bankFragmentPresenter = new BankFragmentPresenter(getActivity(), transaction);
        bankFragmentPresenter.attachView(this);
        ButterKnife.bind(this, view);
        bankButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bankButton:
                transaction.setBank(bank);
                gotoInstallmentsFragment(transaction);
                break;
        }
    }

    @Override
    public void onBackPressedCallback() {
        bankFragmentPresenter.showExitAlert();
    }

    @Override
    public void setBankAdapter(BankAdapter adapter) {
        adapter.setItemClickListener(new BankAdapter.onRecyclerViewItemClickListener() {
            @Override
            public void onItemClickListener(View view, Bank selectedBank) {
                bankButton.setVisibility(View.VISIBLE);
                bank = selectedBank;
            }
        });
        bankRecycler.setHasFixedSize(false);
        bankRecycler.setLayoutManager(new LinearLayoutManager(this.getActivity(), LinearLayoutManager.VERTICAL, false));
        bankRecycler.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showDialog(AlertDialog.Builder dialogBuilder) {
        this.dialog  = dialogBuilder.create();
        if (!getActivity().isFinishing()){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                }
            });
        }
    }

    @Override
    public void dismissDialog() {
        if (!getActivity().isFinishing() && dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

    @Override
    public void backHome() {
        ((MainActivity)getActivity()).setMainFragment();
    }

    @Override
    public void gotoInstallmentsFragment(Transaction transaction) {
        InstallmentFragment installmentFragment = InstallmentFragment.newInstance();
        installmentFragment.setTransaction(transaction);
        ((MainActivity)getActivity()).setFragment(installmentFragment);
    }
}
