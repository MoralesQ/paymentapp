package cl.smq.paymentapp.Adapter;

import android.app.Activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smq.paymentapp.Model.PaymentMethod;
import cl.smq.paymentapp.R;


public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentViewHolder> {

    ArrayList<PaymentMethod> paymentMethods;
    Activity activity;
    private int oldselectedPosition = -1;
    private onRecyclerViewItemClickListener itemClickListener;

    public PaymentMethodAdapter(ArrayList<PaymentMethod> paymentMethods, Activity activity) {
        this.paymentMethods = paymentMethods;
        this.activity = activity;
    }

    public void setItemClickListener(onRecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public PaymentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.payment_method_item, viewGroup, false);
        PaymentViewHolder vh = new PaymentViewHolder(view);
        return  vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final PaymentViewHolder holder, final int position) {

        PaymentMethod item = paymentMethods.get(position);
        if (item.isSelected()){
            holder.paymentMethodSelected.setVisibility(View.VISIBLE);
            oldselectedPosition = position;
        }
        else
            holder.paymentMethodSelected.setVisibility(View.GONE);
        Picasso.with(activity.getBaseContext()).load(item.getThumbnail()).into(holder.paymentMethodIcon);
        holder.paymentMethodName.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return (null != paymentMethods ? paymentMethods.size(): 0);
    }


    public class PaymentViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.paymentMethodIconItem) ImageView paymentMethodIcon;
        @BindView(R.id.paymentMethodNameItem) TextView paymentMethodName;
        @BindView(R.id.paymentMethodSelected) ImageView paymentMethodSelected;
        public PaymentViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClickListener(view, paymentMethods.get(getAdapterPosition()));
                    }
                    paymentMethods.get(getAdapterPosition()).setSelected(true);
                    if (oldselectedPosition >= 0 )
                        paymentMethods.get(oldselectedPosition).setSelected(false);
                    notifyDataSetChanged();
                }
            });
        }

    }

    public interface onRecyclerViewItemClickListener {
        void onItemClickListener(View view, PaymentMethod selectedPaymentMethod);
    }

}

