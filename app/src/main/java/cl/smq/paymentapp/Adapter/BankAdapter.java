package cl.smq.paymentapp.Adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cl.smq.paymentapp.Model.Bank;
import cl.smq.paymentapp.R;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.BankViewHolder>{

    ArrayList<Bank> banks;
    Activity activity;
    private int oldselectedPosition = -1;
    private onRecyclerViewItemClickListener itemClickListener;

    public BankAdapter(ArrayList<Bank> banks, Activity activity) {
        this.banks = banks;
        this.activity = activity;
    }

    public void setItemClickListener(onRecyclerViewItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @NonNull
    @Override
    public BankViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.bank_item, viewGroup, false);
        BankViewHolder vh = new BankViewHolder(view);
        return  vh;
    }

    @Override
    public void onBindViewHolder(@NonNull BankViewHolder holder, int position) {
        Bank item = banks.get(position);
        if (item.isSelected()){
            holder.bankSelected.setVisibility(View.VISIBLE);
            oldselectedPosition = position;
        }
        else
            holder.bankSelected.setVisibility(View.GONE);
        Picasso.with(activity.getBaseContext()).load(item.getThumbnail()).into(holder.bankIconItem);
        holder.bankNameItem.setText(item.getName());
    }

    @Override
    public int getItemCount() {
        return (null != banks ? banks.size(): 0);
    }

    public class BankViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.bankIconItem) ImageView bankIconItem;
        @BindView(R.id.bankNameItem) TextView bankNameItem;
        @BindView(R.id.bankSelected) ImageView bankSelected;
        public BankViewHolder(@NonNull final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClickListener(view, banks.get(getAdapterPosition()));
                    }
                    banks.get(getAdapterPosition()).setSelected(true);
                    if (oldselectedPosition >= 0 )
                        banks.get(oldselectedPosition).setSelected(false);
                    notifyDataSetChanged();
                }
            });
        }

    }

    public interface onRecyclerViewItemClickListener {
        void onItemClickListener(View view, Bank selectedBank);
    }
}