package cl.smq.paymentapp.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;

import cl.smq.paymentapp.R;

public class DialogMaker {
    private Activity activity;
    private static DialogMaker instance = null;
    AlertDialog.Builder builder;

    /**
     * Singleton implementation;
     */

    public static DialogMaker getInstance(Activity activity){
        if (instance == null)
            instance = new DialogMaker(activity);
        return instance;
    }

    public DialogMaker(Activity activity) {
        this.activity = activity;
    }

    public AlertDialog.Builder getErrorDialog(int message, DialogInterface.OnClickListener listener){
        return getErrorDialog(activity.getString(message), listener);
    }

    public AlertDialog.Builder getErrorDialog(String message, DialogInterface.OnClickListener listener){
        builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.dialog_ok, listener);
        return builder;
    }

    public AlertDialog.Builder getFinishDialog(int message, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative ){
        builder = new AlertDialog.Builder(activity);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.dialog_ok, positive);
        builder.setNegativeButton(R.string.dialog_cancel, negative);
        return builder;
    }

    public AlertDialog.Builder getTransactionAlert(String tittle, String message, DialogInterface.OnClickListener listener){
        builder = new AlertDialog.Builder(activity);
        builder.setTitle(tittle);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.dialog_ok, listener);
        return builder;
    }
}
