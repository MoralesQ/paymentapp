package cl.smq.paymentapp.Utils;

import android.content.Context;
import android.net.Uri;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VolleyConnection {
    private Context context;
    private String url;
    private int timeOut;
    private int retry;
    private String urlServ;
    private static VolleyConnection instance = null;

    private static final String BASE_URL = "https://api.mercadopago.com/v1/payment_methods" ;
    private static final int TIME_OUT = 1500;
    private static final int RETRY = 3;


    public static VolleyConnection getInstance(Context context) {
        if (instance == null)
            instance = new VolleyConnection(BASE_URL, TIME_OUT, RETRY, context);
        return instance;
    }

    private VolleyConnection(String url, int timeOut, int retry, Context context) {
        this.url = url;
        this.timeOut = timeOut;
        this.retry = retry;
        this.context = context;
    }

    public void jsonArrayRequest(String service, JSONObject jsonObject, VolleyCallback callback){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
    }


    public void serviceRequest(String service, final Map<String, String> mapRequest, final VolleyCallback callback){

        urlServ = "";
        if (!service.equals(""))
            urlServ = url + "/" + service;
        else
            urlServ = url;

        Uri.Builder builder = Uri.parse(urlServ).buildUpon();

        for (Map.Entry<String, String> entry : mapRequest.entrySet()) {
            builder.appendQueryParameter(entry.getKey(), entry.getValue());
        }

        String finalUrl = builder.build().toString();


        StringRequest request = new StringRequest(Request.Method.GET, finalUrl, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jArray = new JSONArray(response);
                    callback.onResponse(jArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error);
            }
        });

        request.setRetryPolicy(new DefaultRetryPolicy(timeOut, retry, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(context);
        rQueue.add(request);
    }

    public interface VolleyCallback {
        void onResponse(Object result);

        void onError(Object error);
    }

}
