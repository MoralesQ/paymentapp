package cl.smq.paymentapp.Presenter;

import android.app.Activity;
import android.content.DialogInterface;

import cl.smq.paymentapp.Adapter.PaymentMethodAdapter;
import cl.smq.paymentapp.Interactor.ItemPaymentListener;
import cl.smq.paymentapp.Interactor.PaymentMethodInteractor;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.DialogMaker;
import cl.smq.paymentapp.View.PaymentMethodView;

public class PaymentMethodPresenter implements Presenter<PaymentMethodView> , ItemPaymentListener {

    PaymentMethodView paymentMethodView;
    Activity activity;
    PaymentMethodInteractor paymentMethodInteractor;

    public PaymentMethodPresenter(Activity activity) {
        this.activity = activity;
        this.paymentMethodInteractor = new PaymentMethodInteractor(this.activity);
    }

    @Override
    public void attachView(PaymentMethodView view) {
        if (view == null)
        throw new IllegalArgumentException("view is Null");
        paymentMethodView = view;
    }

    @Override
    public void detachView() {
        paymentMethodView = null;
    }

    @Override
    public void onResume() throws Exception {
        paymentMethodInteractor.paymentItem(this);
    }

    public void showExitAlert(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        paymentMethodView.showDialog(dialogMaker.getFinishDialog(R.string.exitMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                paymentMethodView.dismissDialog();
                paymentMethodView.backHome();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                paymentMethodView.dismissDialog();
            }
        }));
    }

    @Override
    public void onFinished(PaymentMethodAdapter paymentMethodAdapter) throws Exception {
        paymentMethodView.setPaymentAdapter(paymentMethodAdapter);
    }

}
