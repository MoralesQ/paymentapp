package cl.smq.paymentapp.Presenter;

import android.app.Activity;
import android.content.DialogInterface;

import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.DialogMaker;
import cl.smq.paymentapp.View.MainFragmentView;

public class MainFragmentPresenter implements Presenter<MainFragmentView>{

    private Activity activity;
    private MainFragmentView mainFragmentView;

    public MainFragmentPresenter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void attachView(MainFragmentView view) {
        if (view == null)
            throw new IllegalArgumentException("view is Null");
        mainFragmentView = view;

    }

    @Override
    public void detachView() {
        mainFragmentView = null;
    }

    @Override
    public void onResume() throws Exception {

    }

    public void validateAmount(String amount){
        if (amount.equals("")){
            setErrorAmountDialog();
        }else if (Integer.parseInt(amount) <= 0){
            setErrorAmountDialog();
        }else {
            Transaction transaction = new Transaction();
            transaction.setAmount(Integer.parseInt(amount));
            mainFragmentView.goToPaymentMethodFragment(transaction);
        }
    }

    private void setErrorAmountDialog(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        mainFragmentView.showDialog(dialogMaker.getErrorDialog(R.string.amount_error, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mainFragmentView.dismissDialog();
            }
        }));
    }

    public void showExitAlert(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        mainFragmentView.showDialog(dialogMaker.getFinishDialog(R.string.exitMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mainFragmentView.dismissDialog();
                mainFragmentView.backHome();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mainFragmentView.dismissDialog();
            }
        }));
    }
}
