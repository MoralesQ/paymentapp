package cl.smq.paymentapp.Presenter;

public interface Presenter<V> {
    void attachView(V view);
    void detachView();
    void onResume() throws Exception;
}