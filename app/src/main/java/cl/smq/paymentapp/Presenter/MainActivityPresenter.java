package cl.smq.paymentapp.Presenter;

import android.app.Activity;

import cl.smq.paymentapp.Fragment.MainFragment;
import cl.smq.paymentapp.Fragment.PaymentMethodFragment;
import cl.smq.paymentapp.View.MainActivityView;

public class MainActivityPresenter implements Presenter<MainActivityView> {

    private MainActivityView mainActivityView;
    private Activity activity;

    public MainActivityPresenter(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void attachView(MainActivityView view) {
        if (view == null)
            throw new IllegalArgumentException("view is Null");
        mainActivityView = view;
        setMainFragment();
    }

    @Override
    public void detachView() {
        mainActivityView = null;
    }

    @Override
    public void onResume() throws Exception {
        setMainFragment();
    }

    public void setMainFragment(){
        MainFragment fragment = MainFragment.newInstance();
        mainActivityView.setFragment(fragment);
    }

    public void setPaymentMethodFragment(){
        PaymentMethodFragment fragment = PaymentMethodFragment.newInstance();
        mainActivityView.setFragment(fragment);

    }

}
