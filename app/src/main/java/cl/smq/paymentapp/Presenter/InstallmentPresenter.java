package cl.smq.paymentapp.Presenter;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import cl.smq.paymentapp.Interactor.InstallmentInteractor;
import cl.smq.paymentapp.Interactor.ItemInstallmentListener;
import cl.smq.paymentapp.Model.Installment;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.DialogMaker;
import cl.smq.paymentapp.View.InstallmentView;

public class InstallmentPresenter implements Presenter<InstallmentView> , ItemInstallmentListener {

    InstallmentView installmentView;
    Activity activity;
    Transaction transaction;
    InstallmentInteractor installmentInteractor;

    public InstallmentPresenter(Activity activity, Transaction transaction) {
        this.activity = activity;
        this.transaction = transaction;
        this.installmentInteractor = new InstallmentInteractor(activity, transaction);
    }



    @Override
    public void attachView(InstallmentView view) {
        if (view == null)
            throw new IllegalArgumentException("view is Null");
        installmentView = view;
    }

    @Override
    public void detachView() {
        installmentView = null;
    }

    @Override
    public void onResume() throws Exception {
        installmentInteractor.installmentItem(this);
    }

    @Override
    public void onFinished(ArrayList<Installment> installmentAdapter) throws Exception {
        if (installmentAdapter.size() < 1){
            DialogMaker dialogMaker = DialogMaker.getInstance(activity);
            installmentView.showDialog(dialogMaker.getErrorDialog(R.string.installment_error, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    installmentView.dismissDialog();
                    installmentView.backHome();
                }
            }));
        }else{
            ArrayAdapter userAdapter = new ArrayAdapter(activity.getBaseContext(), R.layout.spinner, installmentAdapter);
            installmentView.setSpinnerAdapter(userAdapter);

        }
    }

    private void setErrorInstallmentDialog(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        installmentView.showDialog(dialogMaker.getErrorDialog(R.string.installment_selection_error, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                installmentView.dismissDialog();
            }
        }));
    }

    public void showExitAlert(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        installmentView.showDialog(dialogMaker.getFinishDialog(R.string.exitMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                installmentView.dismissDialog();
                installmentView.backHome();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                installmentView.dismissDialog();
            }
        }));
    }

    public void showTransactionAlert(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        installmentView.showDialog(dialogMaker.getTransactionAlert(activity.getString(R.string.installment_resume),transaction.getTransaccionResume(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                installmentView.backHome();
                installmentView.dismissDialog();
            }
        }));

    }

    public void validateInstallment(Installment installment){
        if (installment.getInstallments() > 0){
            transaction.setInstallment(installment);
            installmentView.showInstallmentMessage();
        }else {
            setErrorInstallmentDialog();
        }

    }
}
