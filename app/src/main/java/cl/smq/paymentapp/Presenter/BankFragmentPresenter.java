package cl.smq.paymentapp.Presenter;

import android.app.Activity;
import android.content.DialogInterface;

import cl.smq.paymentapp.Adapter.BankAdapter;
import cl.smq.paymentapp.Interactor.BankInteractor;
import cl.smq.paymentapp.Interactor.ItemBankListener;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.DialogMaker;
import cl.smq.paymentapp.View.BankView;

public class BankFragmentPresenter  implements Presenter<BankView>, ItemBankListener {

    BankView bankView;
    Activity activity;
    Transaction transaction;
    BankInteractor bankInteractor;

    public BankFragmentPresenter(Activity activity, Transaction transaction) {
        this.activity = activity;
        this.transaction = transaction;
        this.bankInteractor = new BankInteractor(this.activity, this.transaction);
    }

    @Override
    public void attachView(BankView view) {
        if (view == null)
            throw new IllegalArgumentException("view is Null");
        bankView = view;
    }

    @Override
    public void detachView() {
        bankView = null;
    }

    @Override
    public void onResume() throws Exception {
        bankInteractor.bankItem(this);

    }

    @Override
    public void onFinished(BankAdapter bankAdapter) throws Exception {
        if (bankAdapter.getItemCount() < 1){
            DialogMaker dialogMaker = DialogMaker.getInstance(activity);
            bankView.showDialog(dialogMaker.getErrorDialog(R.string.bank_error, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    bankView.dismissDialog();
                    bankView.backHome();
                }
            }));
        }else
            bankView.setBankAdapter(bankAdapter);

    }

    public void showExitAlert(){
        DialogMaker dialogMaker = DialogMaker.getInstance(activity);
        bankView.showDialog(dialogMaker.getFinishDialog(R.string.exitMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                bankView.dismissDialog();
                bankView.backHome();
            }
        }, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                bankView.dismissDialog();
            }
        }));
    }

}
