package cl.smq.paymentapp.Interactor;

import cl.smq.paymentapp.Adapter.PaymentMethodAdapter;

public interface ItemPaymentListener {
    void onFinished(PaymentMethodAdapter paymentMethodAdapter) throws Exception;
}
