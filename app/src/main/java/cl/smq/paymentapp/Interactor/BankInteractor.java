package cl.smq.paymentapp.Interactor;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cl.smq.paymentapp.Adapter.BankAdapter;
import cl.smq.paymentapp.Model.Bank;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.VolleyConnection;

public class BankInteractor implements ItemInteractor {

    private Activity activity;
    private Transaction transaction;

    public BankInteractor(Activity activity, Transaction transaction) {
        this.activity = activity;
        this.transaction = transaction;
    }

    @Override
    public void paymentItem(ItemPaymentListener itemPaymentListener) throws Exception {

    }

    @Override
    public void bankItem(final ItemBankListener itemBankListener) throws Exception {
        final ArrayList<Bank> banks = new ArrayList<>();
        getBankAdapter(new VolleyConnection.VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                JSONArray banksArray = (JSONArray) result;
                for (int i = 0; banksArray.length() > i; i++){
                    try {
                        JSONObject j = banksArray.getJSONObject(i);
                        Bank bank = new Bank();
                        bank.setId(j.getString("id"));
                        bank.setName(j.getString("name"));
                        bank.setSucureThumbnail(j.getString("secure_thumbnail"));
                        bank.setThumbnail(j.getString("thumbnail"));
                        banks.add(bank);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    itemBankListener.onFinished(new BankAdapter(banks, activity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Object error) {
                new BankAdapter(new ArrayList<Bank>(), activity);
            }
        });
    }

    @Override
    public void installmentItem(ItemInstallmentListener itemInstallmentListener) throws Exception {

    }

    private void getBankAdapter(VolleyConnection.VolleyCallback volleyCallback){
        HashMap<String, String> params = new HashMap<>();
        params.put(activity.getString(R.string.public_key_name), activity.getString(R.string.public_key));
        params.put(activity.getString(R.string.payment_method_id), transaction.getPaymentMethod().getId());
        VolleyConnection vc = VolleyConnection.getInstance(activity.getBaseContext());
        vc.serviceRequest(activity.getString(R.string.service_card_issuers), params, volleyCallback);
    }
}
