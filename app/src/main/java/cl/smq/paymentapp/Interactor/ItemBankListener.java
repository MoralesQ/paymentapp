package cl.smq.paymentapp.Interactor;

import cl.smq.paymentapp.Adapter.BankAdapter;

public interface ItemBankListener {
    void onFinished(BankAdapter bankAdapter) throws Exception;
}
