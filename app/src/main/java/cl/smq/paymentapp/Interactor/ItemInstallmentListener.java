package cl.smq.paymentapp.Interactor;

import java.util.ArrayList;

import cl.smq.paymentapp.Model.Installment;

public interface ItemInstallmentListener {
    void onFinished(ArrayList<Installment> installmentAdapter) throws Exception;

}
