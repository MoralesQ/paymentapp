package cl.smq.paymentapp.Interactor;

import android.app.Activity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cl.smq.paymentapp.Model.Installment;
import cl.smq.paymentapp.Model.Transaction;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.VolleyConnection;

public class InstallmentInteractor implements ItemInteractor{
    private Activity activity;
    private Transaction transaction;

    public InstallmentInteractor(Activity activity, Transaction transaction) {
        this.activity = activity;
        this.transaction = transaction;
    }

    @Override
    public void paymentItem(ItemPaymentListener itemPaymentListener) throws Exception {

    }

    @Override
    public void bankItem(ItemBankListener itemBankListener) throws Exception {

    }

    @Override
    public void installmentItem(final ItemInstallmentListener itemInstallmentListener) throws Exception {
        final ArrayList<Installment> installments = new ArrayList<>();
        installments.add(new Installment());
        getInstallmentAdapter(new VolleyConnection.VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                JSONArray installmentArray = (JSONArray) result;
                for (int i = 0; installmentArray.length() > i; i++){
                    try {
                        JSONObject j = installmentArray.getJSONObject(i);
                        String paymentMethodId =  j.getString("payment_method_id");
                        String paymentTypeId = j.getString("payment_type_id");
                        //installment.setPaymentMethodId(j.getString("payment_method_id"));
                        //installment.setPaymentTypeId(j.getString("payment_type_id"));
                        JSONArray pc  = j.getJSONArray("payer_costs");
                        for (int x = 0; pc.length() > x; x++ ){
                            JSONObject ji = pc.getJSONObject(x);
                            Installment installment = new Installment();
                            installment.setPaymentMethodId(paymentMethodId);
                            installment.setPaymentTypeId(paymentTypeId);
                            installment.setInstallments(ji.getInt("installments"));
                            installment.setMinAllowedAmount(ji.getInt("min_allowed_amount"));
                            installment.setMaxAllowedAmount(ji.getInt("max_allowed_amount"));
                            installment.setRecommendedMessage(ji.getString("recommended_message"));
                            installments.add(installment);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    itemInstallmentListener.onFinished(installments);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Object error) {
                new ArrayList<Installment>();
            }
        });

    }

    private void getInstallmentAdapter(VolleyConnection.VolleyCallback volleyCallback){
        HashMap<String, String> params = new HashMap<>();
        params.put(activity.getString(R.string.public_key_name), activity.getString(R.string.public_key));
        params.put(activity.getString(R.string.amount), String.valueOf(transaction.getAmount()));
        params.put(activity.getString(R.string.payment_method_id), transaction.getPaymentMethod().getId());
        params.put(activity.getString(R.string.issuer_id), transaction.getBank().getId());
        VolleyConnection vc = VolleyConnection.getInstance(activity.getBaseContext());
        vc.serviceRequest(activity.getString(R.string.service_installments), params, volleyCallback);
    }
}
