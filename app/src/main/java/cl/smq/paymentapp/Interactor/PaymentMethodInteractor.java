package cl.smq.paymentapp.Interactor;

import android.app.Activity;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cl.smq.paymentapp.Adapter.PaymentMethodAdapter;
import cl.smq.paymentapp.Model.PaymentMethod;
import cl.smq.paymentapp.R;
import cl.smq.paymentapp.Utils.VolleyConnection;

public class PaymentMethodInteractor implements ItemInteractor {
    private Activity activity;

    public PaymentMethodInteractor(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void paymentItem(final ItemPaymentListener itemPaymentListener) throws Exception {
        final ArrayList<PaymentMethod> paymentMethods = new ArrayList<>();
        getPaymentAdapter(new VolleyConnection.VolleyCallback() {
            @Override
            public void onResponse(Object result) {
                JSONArray paymentMethodArray = (JSONArray) result;
                for (int i = 0; paymentMethodArray.length() > i; i++){
                    try {
                        JSONObject j = paymentMethodArray.getJSONObject(i);
                        if (j.getString("payment_type_id").equals("credit_card")){
                            PaymentMethod payment = new PaymentMethod();
                            payment.setId(j.getString("id"));
                            payment.setName(j.getString("name"));
                            payment.setPaymentTyeId(j.getString("payment_type_id"));
                            payment.setStatus(j.getString("status"));
                            payment.setSecureThumbnail(j.getString("secure_thumbnail"));
                            payment.setThumbnail(j.getString("thumbnail"));
                            payment.setMinAllowedAmount(j.getInt("min_allowed_amount"));
                            payment.setMaxAllowedAmount(j.getInt("max_allowed_amount"));
                            paymentMethods.add(payment);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    itemPaymentListener.onFinished(new PaymentMethodAdapter(paymentMethods, activity));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Object error) {
                new PaymentMethodAdapter(new ArrayList<PaymentMethod>(),activity);
            }
        });
    }

    @Override
    public void bankItem(ItemBankListener itemBankListener) throws Exception {

    }

    @Override
    public void installmentItem(ItemInstallmentListener itemInstallmentListener) throws Exception {

    }

    private void getPaymentAdapter(VolleyConnection.VolleyCallback volleyCallback){
        HashMap<String, String> params = new HashMap<>();
        params.put(activity.getString(R.string.public_key_name), activity.getString(R.string.public_key));
        VolleyConnection vc = VolleyConnection.getInstance(activity.getBaseContext());
        vc.serviceRequest("", params, volleyCallback);
    }
}
