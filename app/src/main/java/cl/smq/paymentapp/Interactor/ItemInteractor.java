package cl.smq.paymentapp.Interactor;

public interface ItemInteractor {
    void paymentItem(ItemPaymentListener itemPaymentListener) throws Exception;
    void bankItem(ItemBankListener itemBankListener) throws Exception;
    void installmentItem(ItemInstallmentListener itemInstallmentListener) throws Exception;
}
